/**
 * @file offb_node.cpp
 * @brief Offboard control example node, written with MAVROS version 0.19.x, PX4 Pro Flight
 * Stack and tested in Gazebo SITL
 */

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include "quadrotor_msgs/PositionCommand.h"
#include <mavros_msgs/PositionTarget.h>

mavros_msgs::State current_state;
void state_cb(const mavros_msgs::State::ConstPtr& msg){
    current_state = *msg;
    std::cout << "Got state " << std::endl;
}

static bool local_pose_received = false;
mavros_msgs::PositionTarget poscmd;
bool message_received = false;
static double h_speed;
static double v_speed;
static double turn_speed;

void local_odom_cb(const geometry_msgs::PoseStampedConstPtr& msg){
  local_pose_received = true;
}


void position_cmd_cb(const quadrotor_msgs::PositionCommand::ConstPtr& cmd){


    poscmd.header.stamp =  ros::Time::now();
    poscmd.header.frame_id = "world";
    poscmd.header.seq += 1;
    poscmd.coordinate_frame = mavros_msgs::PositionTarget::FRAME_BODY_NED;
    poscmd.type_mask = mavros_msgs::PositionTarget::IGNORE_YAW_RATE;

    poscmd.position = cmd->position;

    poscmd.velocity = cmd->velocity;

    poscmd.acceleration_or_force = cmd->acceleration;

    poscmd.yaw = cmd->yaw;


    // std::cout<<"aa"<<poscmd.position.x<<std::endl;
    // std::cout<<"bb"<<poscmd.position.y<<std::endl;
    // std::cout<<"cc"<<poscmd.position.z<<std::endl;

    message_received = true;
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "message_conversion");
    ros::NodeHandle nh;

    nh.getParam("velocity_horizon", h_speed);
    nh.getParam("velocity_vertical", v_speed);
    nh.getParam("angular_velocity_turn", turn_speed);

    ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>
            ("mavros/state", 10, state_cb);
    ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>
            ("mavros/setpoint_position/local", 10);
    ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>
            ("mavros/cmd/arming");
    ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
            ("mavros/set_mode");

    ros::Subscriber local_odom_sub = nh.subscribe<geometry_msgs::PoseStamped>
      ("/mavros/local_position/pose", 10, local_odom_cb);


    ros::Subscriber pose_sub = nh.subscribe<quadrotor_msgs::PositionCommand>
            ("/drone_0_planning/pos_cmd", 10, position_cmd_cb);

    ros::Publisher local_pos_cmd = nh.advertise<mavros_msgs::PositionTarget>
            ("/mavros/setpoint_raw/local", 10);

    //the setpoint publishing rate MUST be faster than 2Hz
    ros::Rate rate(20.0);

    //wait for FCU connection
    std::cout << "Waiting for FCU connection " << std::endl;
    while(ros::ok() && !current_state.connected){
        ros::spinOnce();
        rate.sleep();
        std::cout << "Waiting for FCU connection " << std::endl;
    }
    while(local_pose_received==false)
    {
        ros::spinOnce();
        rate.sleep();
        std::cout << "Waiting for Local Position" << std::endl;
    }

    mavros_msgs::SetMode offb_set_mode;
    offb_set_mode.request.custom_mode = "OFFBOARD";

    mavros_msgs::CommandBool arm_cmd;
    arm_cmd.request.value = true;

    ros::Time last_request = ros::Time::now();

    while(ros::ok()){
        if( current_state.mode != "OFFBOARD" && (ros::Time::now() - last_request > ros::Duration(1.0))){
            if( set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent){
                ROS_INFO("Offboard enabled");
            }
            last_request = ros::Time::now();
        } 
        else 
        {
            if(!current_state.armed && (ros::Time::now() - last_request > ros::Duration(1.0)))
            {
                if( arming_client.call(arm_cmd) && arm_cmd.response.success)
                {

                    ROS_INFO("Vehicle armed");

                    geometry_msgs::PoseStamped pose;
                    pose.pose.position.x = 0;
                    pose.pose.position.y = 0;
                    pose.pose.position.z = 1.5;

                    //send a few setpoints before starting
                    for(int i = 100; ros::ok() && i > 0; --i){
                        local_pos_pub.publish(pose);
                        ros::spinOnce();
                        rate.sleep();
                    }

                    //Initialze conversion message
                    poscmd.header.stamp =  ros::Time::now();
                    poscmd.header.frame_id = "world";
                    poscmd.header.seq += 1;
                    poscmd.coordinate_frame = mavros_msgs::PositionTarget::FRAME_BODY_NED;
                    poscmd.type_mask = mavros_msgs::PositionTarget::IGNORE_VX | mavros_msgs::PositionTarget::IGNORE_VY | mavros_msgs::PositionTarget::IGNORE_VZ |
                                        mavros_msgs::PositionTarget::IGNORE_AFX | mavros_msgs::PositionTarget::IGNORE_AFY | mavros_msgs::PositionTarget::IGNORE_AFZ | 
                                        mavros_msgs::PositionTarget::IGNORE_YAW | mavros_msgs::PositionTarget::IGNORE_YAW_RATE | 
                                        mavros_msgs::PositionTarget::FORCE;

                    poscmd.position = pose.pose.position;
                }
                last_request = ros::Time::now();
            }
        }

        //local_pos_pub.publish(pose);
        //local_pos_cmd.publish(u);
        if(message_received){
            local_pos_cmd.publish(poscmd);
            message_received = false;
        }
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
